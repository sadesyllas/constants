defmodule Constants do
  @moduledoc """
  Provides a simple and easy to use API for defining enums in Elixir.

  A typical use case such as:

  ```elixir
  defmodule FoodTaste do
    @moduledoc false

    use Constants,
      nice: 1,
      bad: 2,
      # spicy is not associated with a numeric value but we wnat to use it's uniqueness
      spicy: nil,
      # salty is not associated with a numeric value but we wnat to use it's uniqueness
      salty: {nil, foo: 1, bar: 2}
  end
  ```

  will produce the following module code:

  ```elixir
  defmodule FoodTaste do
    @moduledoc false

    @typep constants :: [atom | {atom, pos_integer} | {atom, {pos_integer, keyword} | {atom, keyword}}]

    @type t :: FoodTaste.Nice | FoodTaste.Bad | FoodTaste.Spicy | FoodTaste.Salty

    defguard is_valid(constant) when constant in [FoodTaste.Nice, FoodTaste.Bad, FoodTaste.Spicy, FoodTaste.Salty]

    defguard is_valid_name(name) when name in [:nice, :bad, :spicy, :salty]

    defguard is_valid_value(value) when value in [1, 2]

    defmacro __using__(opts) do
      default_as =
        __MODULE__
        |> Module.split()
        |> List.last()
        |> String.to_atom()
        |> (fn default_as ->
              {:__aliases__, [], [default_as]}
            end).()

      as = Keyword.get(opts, :as, default_as)

      quote location: :keep do
        require unquote(__MODULE__), as: unquote(as)
      end
    end

    @doc \"\"\"
    Returns the constants initially passed to `use Constants` so that another
    constant can be defined in terms of the one on which `constants` is being
    invoked.
    \"\"\"
    @spec constants() :: constants
    def constants(), do: [
      nice: 1,
      bad: 2,
      spicy: nil,
      salty: {nil, foo: 1, bar: 2}
    ]

    defmodule FoodTaste.Nice do
      @moduledoc false

      @doc \"\"\"
      Returns the name with which this enum constant has been defined.
      \"\"\"
      @spec name() :: atom
      def name(), do: :nice

      @doc \"\"\"
      Returns the value associated with this enum constant.
      \"\"\"
      @spec value() :: pos_integer | nil
      def value(), do: 1
    end

    defmodule FoodTaste.Bad do
      @moduledoc false

      @doc \"\"\"
      Returns the name with which this enum constant has been defined.
      \"\"\"
      @spec name() :: atom
      def name(), do: :bad

      @doc \"\"\"
      Returns the value associated with this enum constant.
      \"\"\"
      @spec value() :: pos_integer | nil
      def value(), do: 2
    end

    defmodule FoodTaste.Spicy do
      @moduledoc false

      @doc \"\"\"
      Returns the name with which this enum constant has been defined.
      \"\"\"
      @spec name() :: atom
      def name(), do: :spicy

      @doc \"\"\"
      Returns the value associated with this enum constant.
      \"\"\"
      @spec value() :: pos_integer | nil
      def value(), do: nil
    end

    defmodule FoodTaste.Salty do
      @moduledoc false

      @doc \"\"\"
      Returns the name with which this enum constant has been defined.
      \"\"\"
      @spec name() :: atom
      def name(), do: :salty

      @doc \"\"\"
      Returns the value associated with this enum constant.
      \"\"\"
      @spec value() :: pos_integer | nil
      def value(), do: nil
    end

    defmacro nice(), do: FoodTaste.Nice

    defmacro bad(), do: FoodTaste.Bad

    defmacro spicy(), do: FoodTaste.Spicy

    defmacro salty(), do: FoodTaste.Salty

    defmacro name_of_nice(), do: :nice

    defmacro name_of_bad(), do: :bad

    defmacro name_of_spicy(), do: :spicy

    defmacro name_of_salty(), do: :salty

    defmacro value_of_nice(), do: 1

    defmacro value_of_bad(), do: 2

    defmacro value_of_spicy(), do: 3

    defmacro value_of_salty(), do: 4

    @doc \"\"\"
    Maps atom names to module aliases which represent the enum's values.
    \"\"\"
    @spec from_name(atom) :: module | nil
    def from_name(name)
    def from_name(:nice), do: FoodTaste.Nice
    def from_name(:bad), do: FoodTaste.Bad
    def from_name(:spicy), do: FoodTaste.Spicy
    def from_name(:salty), do: FoodTaste.Salty
    def from_name(_), do: nil

    @doc \"\"\"
    Maps numeric values to module aliases which represent the enum's values.
    \"\"\"
    @spec from_value(pos_integer) :: module | nil
    def from_value(value)
    def from_value(1), do: FoodTaste.Nice
    def from_value(2), do: FoodTaste.Bad
    def from_value(_), do: nil

    @doc \"\"\"
    Maps atom names to numeric values, which represent the backing numeric values of the enum's constants.
    \"\"\"
    @spec value_from_name(atom) :: pos_integer | nil
    def value_from_name(name)
    def value_from_name(:nice), do: 1
    def value_from_name(:bad), do: 2
    def value_from_name(_), do: nil

    @doc \"\"\"
    Maps atom names to numeric values, which represent the backing numeric values of the enum's constants.
    \"\"\"
    @spec name_from_value(pos_integer) :: atom | nil
    def name_from_value(value)
    def name_from_value(1), do: :nice
    def name_from_value(2), do: :bad
    def name_from_value(_), do: nil
  end
  ```

  The `defmacro`s that start with `name_of_` and `value_of_`, can be used to
  pattern match on arguments of a function expecting values of constants
  created by this macro.

  A simple example usage of this macro is as follows:

  ```elixir
  defmodule Plate do
    @moduledoc false

    use FoodTaste

    IO.puts("We can refer to constant values like \#{FoodTaste.salty()} during compilation.")

    def eat(FoodTaste.nice() = taste, n) do
      IO.puts("I will eat a food with a very \#{taste.name()} taste \#{n} times")
    end

    def eat(FoodTaste.bad() = taste, n) do
      IO.puts("I have eaten food with a \#{taste.name()} taste \#{n} times")
    end
  end
  ```
  """

  defmacro __using__([]) do
    quote do
    end
  end

  defmacro __using__(constants) when is_list(constants) do
    using(constants, __CALLER__.module)
  end

  defmacro __using__(constants) do
    case Code.eval_quoted(constants, [], __CALLER__) do
      {constants, _} when is_list(constants) ->
        using(constants, __CALLER__.module)

      _ ->
        raise ArgumentError, "`constants` did not evaluate to a list: #{inspect(constants)}"
    end
  end

  defp using(constants, caller_module) when is_list(constants) and is_atom(caller_module) do
    using_ast = get_using_ast()

    constants_ast = get_constants_ast(constants)

    reducer = get_constants_reducer(caller_module)

    {
      names,
      values,
      module_names,
      defmodule_ast,
      defmacro_ast,
      defmacro_name_of_ast,
      defmacro_value_of_ast,
      from_name_ast,
      from_value_ast,
      value_from_name_ast,
      name_from_value_ast,
      _unique_entries
    } =
      Enum.reduce(constants, {[], [], [], [], [], [], [], [], [], [], [], MapSet.new()}, fn
        # {name, value} where value can be either {value, metadata} or metadata
        {_, _} = name_value, acc -> reducer.(name_value, acc)
        # only name
        name, acc -> reducer.({name, nil}, acc)
      end)

    [
      get_typespec_ast(module_names),
      get_defguard_is_valid_ast(module_names),
      get_defguard_is_valid_name_ast(names),
      get_defguard_is_valid_value_ast(values),
      using_ast,
      constants_ast,
      defmodule_ast,
      defmacro_ast,
      defmacro_name_of_ast,
      defmacro_value_of_ast,
      get_combined_from_name_ast(from_name_ast),
      get_combined_from_value_ast(from_value_ast),
      get_combined_value_from_name_ast(value_from_name_ast),
      get_combined_name_from_value_ast(name_from_value_ast)
    ]
  end

  # generate a `defmacro __using__` to make it easier to both require and alias the constants module
  defp get_using_ast() do
    quote location: :keep, bind_quoted: [] do
      defmacro __using__(opts) do
        default_as =
          __MODULE__
          |> Module.split()
          |> List.last()
          |> String.to_atom()
          |> (fn default_as ->
                {:__aliases__, [], [default_as]}
              end).()

        as = Keyword.get(opts, :as, default_as)

        quote location: :keep do
          require unquote(__MODULE__), as: unquote(as)
        end
      end
    end
  end

  # generate a `defmacro constants` to make it to allow reusing the constants from one module into another
  defp get_constants_ast(constants) do
    quote location: :keep do
      @doc """
      Returns the constants initially passed to `use Constants` so that another
      constant can be defined in terms of the one on which `constants` is being
      invoked.
      """
      @spec constants() :: constants
      def constants(), do: unquote(constants)
    end
  end

  defp get_constants_reducer(caller_module) do
    fn {name, value},
       {
         names,
         values,
         module_names,
         defmodule_ast,
         defmacro_ast,
         defmacro_name_of_ast,
         defmacro_value_of_ast,
         from_name_ast,
         from_value_ast,
         value_from_name_ast,
         name_from_value_ast,
         unique_entries
       } ->
      {value, metadata} =
        case value do
          # both value and metadata
          {_value, metadata} = value_metadata when is_list(metadata) -> value_metadata
          # only metadata
          value when is_list(value) -> {nil, value}
          # only value
          value -> {value, []}
        end

      if MapSet.member?(unique_entries, {:name, name}) do
        raise ArgumentError, "Duplicate name found: #{name}"
      end

      unique_entries = MapSet.put(unique_entries, {:name, name})

      names = [name | names]

      {values, unique_entries} =
        if value === nil do
          {values, unique_entries}
        else
          if MapSet.member?(unique_entries, {:value, value}) do
            raise ArgumentError, "Duplicate value found for #{name}: #{value}"
          end

          unique_entries = MapSet.put(unique_entries, {:value, value})

          values = [value | values]

          {values, unique_entries}
        end

      module_name = get_module_name(name, caller_module)
      module_names = [module_name | module_names]

      defmodule_ast_new = get_defmodule_ast(name, value, metadata, module_name)
      defmodule_ast = [defmodule_ast_new | defmodule_ast]

      defmacro_ast_new = get_defmacro_ast(name, module_name)
      defmacro_ast = [defmacro_ast_new | defmacro_ast]

      defmacro_name_of_ast_new = get_defmacro_name_of_ast(name)
      defmacro_name_of_ast = [defmacro_name_of_ast_new | defmacro_name_of_ast]

      defmacro_value_of_ast_new = get_defmacro_value_of_ast(name, value)
      defmacro_value_of_ast = [defmacro_value_of_ast_new | defmacro_value_of_ast]

      from_name_ast_new = get_from_name_ast(name, module_name)
      from_name_ast = [from_name_ast_new | from_name_ast]

      from_value_ast_new = get_from_value_ast(value, module_name)
      from_value_ast = [from_value_ast_new | from_value_ast]

      value_from_name_ast_new = get_value_from_name_ast(name, value)
      value_from_name_ast = [value_from_name_ast_new | value_from_name_ast]

      name_from_value_ast_new = get_name_from_value_ast(name, value)
      name_from_value_ast = [name_from_value_ast_new | name_from_value_ast]

      {
        names,
        values,
        module_names,
        defmodule_ast,
        defmacro_ast,
        defmacro_name_of_ast,
        defmacro_value_of_ast,
        from_name_ast,
        from_value_ast,
        value_from_name_ast,
        name_from_value_ast,
        unique_entries
      }
    end
  end

  # generate a `@type t :: A` typespec
  defp get_typespec_ast([module_name]) do
    ast =
      quote location: :keep do
        @type t :: unquote(module_name)
      end

    [get_typespec_common_ast(), ast]
  end

  # generate a `@type t :: A | B | ...` typespec
  defp get_typespec_ast(module_names) do
    module_names = Enum.reverse(module_names)
    typespec_pipe_ast = Enum.take(module_names, -2)
    typespec_pipe_ast = {:|, [], typespec_pipe_ast}

    typespec_pipe_ast =
      module_names
      |> Enum.drop(-2)
      |> Enum.reverse()
      |> Enum.reduce(typespec_pipe_ast, fn module_name, typespec_pipe_ast ->
        {:|, [], [module_name, typespec_pipe_ast]}
      end)

    ast =
      quote location: :keep do
        @type t :: unquote(typespec_pipe_ast)
      end

    [get_typespec_common_ast(), ast]
  end

  defp get_typespec_common_ast() do
    quote location: :keep do
      @typep constants :: [
               atom | {atom, pos_integer} | {atom, {pos_integer, keyword} | {atom, keyword}}
             ]
    end
  end

  # generate a `defguard is_valid(constant) when constant in [A, B, ...]`
  defp get_defguard_is_valid_ast(module_names) do
    alias_list_ast =
      module_names
      |> Enum.reverse()
      |> Enum.map(fn module_name ->
        {:__aliases__, [], [module_name]}
      end)

    quote location: :keep do
      defguard is_valid(constant) when constant in unquote(alias_list_ast)
    end
  end

  # generate a `defguard is_valid_name(name) when name in [:a, :b, ...]`
  defp get_defguard_is_valid_name_ast(names) do
    names_list_ast = Enum.reverse(names)

    quote location: :keep do
      defguard is_valid_name(name) when name in unquote(names_list_ast)
    end
  end

  # generate a `defguard is_valid_value(value) when value in [1, 2, ...]`
  defp get_defguard_is_valid_value_ast(values) do
    values_list_ast = Enum.reverse(values)

    quote location: :keep do
      defguard is_valid_value(value) when value in unquote(values_list_ast)
    end
  end

  # generate a `defmodule` to hold the helper functions of the constant value
  defp get_defmodule_ast(name, value, metadata, module_name) do
    reducer =
      (fn ->
         constant_name = name

         fn {name, value}, {metadata_ast, unique_entries} ->
           if MapSet.member?(unique_entries, {:name, name}) do
             raise ArgumentError, "Duplicate name found in metadata of #{constant_name}: #{name}"
           end

           unique_entries = MapSet.put(unique_entries, {:name, name})

           ast =
             quote location: :keep do
               @doc """
               Returns the value of the containing constant value's metadata property `#{
                 unquote(name)
               }`.
               """
               @spec unquote(name)() :: term
               def unquote(name)(), do: unquote(value)
             end

           metadata_ast = [ast | metadata_ast]

           {metadata_ast, unique_entries}
         end
       end).()

    {metadata_ast, _unique_entries} = Enum.reduce(metadata, {[], MapSet.new()}, reducer)

    quote location: :keep do
      defmodule unquote(module_name) do
        @moduledoc false

        @doc """
        Returns the name with which this enum constant has been defined.
        """
        @spec name() :: atom
        def name(), do: unquote(name)

        @doc """
        Returns the value associated with this enum constant.
        """
        @spec value() :: pos_integer | nil
        def value(), do: unquote(value)

        unquote(metadata_ast)
      end
    end
  end

  # generate the `defmacro` which will return constant value based on the name of the `defmacro`
  defp get_defmacro_ast(name, module_name) do
    quote location: :keep, bind_quoted: [name: name, module_name: module_name] do
      defmacro unquote(name)(), do: unquote(module_name)
    end
  end

  # generate the `defmacro` that can be used to pattern match against a constant value's name when defining a `def`
  # (`def` is a macro so this `defmacro` exists to expand the call to the constant value, which is passed as
  # a quoted expression to the `def`)
  defp get_defmacro_name_of_ast(name) do
    defmacro_name = ("name_of_" <> to_string(name)) |> String.to_atom()

    quote location: :keep, bind_quoted: [defmacro_name: defmacro_name, name: name] do
      defmacro unquote(defmacro_name)(), do: unquote(name)
    end
  end

  # generate the `defmacro` that can be used to pattern match against a constant value's numeric value
  # when defining a `def` (`def` is a macro so this `defmacro` exists to expand the call to the constant value,
  # which is passed as a quoted expression to the `def`)
  defp get_defmacro_value_of_ast(name, value) do
    defmacro_name = ("value_of_" <> to_string(name)) |> String.to_atom()

    quote location: :keep, bind_quoted: [defmacro_name: defmacro_name, value: value] do
      defmacro unquote(defmacro_name)(), do: unquote(value)
    end
  end

  # generate a function clause to get the constant value from it's numeric value
  defp get_from_value_ast(value, module_name)

  defp get_from_value_ast(nil, _module_name) do
    quote do
    end
  end

  defp get_from_name_ast(name, module_name) do
    quote location: :keep do
      def from_name(unquote(name)), do: unquote(module_name)
    end
  end

  # combine ASTs for `from_name`
  defp get_combined_from_name_ast(from_name_ast) do
    from_name_head_ast =
      quote location: :keep do
        @doc """
        Maps atom names to module aliases which represent the enum's values.
        """
        @spec from_name(atom) :: module | nil
        def from_name(name)
      end

    from_name_implementation_ast =
      if from_name_ast === [] do
        []
      else
        quote location: :keep do
          unquote(from_name_ast)
        end
      end

    from_name_tail_ast =
      quote location: :keep do
        def from_name(_), do: nil
      end

    quote location: :keep do
      unquote(from_name_head_ast)

      unquote(from_name_implementation_ast)

      unquote(from_name_tail_ast)
    end
  end

  # combine ASTs for `from_value`
  defp get_combined_from_value_ast(from_value_ast) do
    from_value_head_ast =
      quote location: :keep do
        @doc """
        Maps numeric values to module aliases which represent the enum's values.
        """
        @spec from_value(pos_integer) :: module | nil
        def from_value(value)
      end

    from_value_implementation_ast =
      if from_value_ast === [] do
        []
      else
        quote location: :keep do
          unquote(from_value_ast)
        end
      end

    from_value_tail_ast =
      quote location: :keep do
        def from_value(_), do: nil
      end

    quote location: :keep do
      unquote(from_value_head_ast)

      unquote(from_value_implementation_ast)

      unquote(from_value_tail_ast)
    end
  end

  # combine ASTs for `value_from_name`
  defp get_combined_value_from_name_ast(value_from_name_ast) do
    value_from_name_head_ast =
      quote location: :keep do
        @doc """
        Maps atom names to numeric values, which represent the backing numeric values of the enum's constants.
        """
        @spec value_from_name(atom) :: pos_integer | nil
        def value_from_name(name)
      end

    value_from_name_implementation_ast =
      if value_from_name_ast === [] do
        []
      else
        quote location: :keep do
          unquote(value_from_name_ast)
        end
      end

    value_from_name_tail_ast =
      quote location: :keep do
        def value_from_name(_), do: nil
      end

    quote location: :keep do
      unquote(value_from_name_head_ast)

      unquote(value_from_name_implementation_ast)

      unquote(value_from_name_tail_ast)
    end
  end

  # combine ASTs for `name_from_value`
  defp get_combined_name_from_value_ast(name_from_value_ast) do
    name_from_value_head_ast =
      quote location: :keep do
        @doc """
        Maps numeric values, which represent the backing numeric values of the enum's constants, to atom names.
        """
        @spec name_from_value(pos_integer) :: atom | nil
        def name_from_value(value)
      end

    name_from_value_implementation_ast =
      if name_from_value_ast === [] do
        []
      else
        quote location: :keep do
          unquote(name_from_value_ast)
        end
      end

    name_from_value_tail_ast =
      quote location: :keep do
        def name_from_value(_), do: nil
      end

    quote location: :keep do
      unquote(name_from_value_head_ast)

      unquote(name_from_value_implementation_ast)

      unquote(name_from_value_tail_ast)
    end
  end

  defp get_from_value_ast(value, module_name) do
    quote location: :keep do
      def from_value(unquote(value)), do: unquote(module_name)
    end
  end

  defp get_value_from_name_ast(name, value) do
    quote location: :keep do
      def value_from_name(unquote(name)), do: unquote(value)
    end
  end

  # generate a function clause to get the constant's name from it's numeric value
  defp get_name_from_value_ast(name, value)

  defp get_name_from_value_ast(_name, nil) do
    quote do
    end
  end

  defp get_name_from_value_ast(name, value) do
    quote location: :keep do
      def name_from_value(unquote(value)), do: unquote(name)
    end
  end

  # construct the module name of the alias which will represent a constant value
  defp get_module_name(name, caller_module) do
    module = name |> to_string() |> Macro.camelize()

    Module.concat(caller_module, module)
  end
end
