# Constants

A macro to define constant values in a module.

The module becomes an enumeration of said values where the values can be used
as compile time constants and can also be pattern matched on.

Plus, several convenience functions have been defined to turn a name into a
numeric value and vice versa, based on variables that are not compile time
constants.

Finally, metadata can be attached to constant values which take the form of
functions that can be called on the related constant value and yield constant
values as well.

This library takes advantage of `defmacro`s and peep hole optimizations done
when BEAM code is emitted.

For more information, please, produce the project's documentation, as explained
below, and refer to the documentation of module `Constants`.

Plus, you can refer to the test file `test/constants_test.exs` for a quick
display of the provided API.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `constants` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:constants, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/constants](https://hexdocs.pm/constants).

