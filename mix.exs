defmodule Constants.MixProject do
  use Mix.Project

  def project do
    [
      app: :constants,
      version: "1.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() === :prod,
      deps: deps(),
      elixirc_paths: elixirc_paths(Mix.env())
    ] ++ docs()
  end

  defp elixirc_paths(:test), do: ["lib", "test/extra"]
  defp elixirc_paths(_), do: ["lib"]

  def application() do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps() do
    [
      {:credo, "~> 1.1", only: [:dev, :test], runtime: false, optional: true},
      {:dialyxir, "~> 1.0.0-rc.6", only: [:dev, :test], runtime: false, optional: true},
      {:ex_doc, "~> 0.21.2", only: :dev, runtime: false, optional: true}
    ]
  end

  defp docs() do
    [
      name: "constants",
      source_url: "https://gitlab.stoiximan.eu/ariadne/lib/constants",
      homepage_url: "https://gitlab.stoiximan.eu/ariadne/lib/constants",
      docs: [
        main: "readme",
        logo: "logo.png",
        extras: ["README.md"]
      ]
    ]
  end
end
