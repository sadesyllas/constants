defmodule ConstantsTest do
  use ExUnit.Case

  use Const, as: MyConst
  use OtherConst

  test "built contant values work" do
    assert MyConst.a() === MyConst.A
    assert MyConst.a().value() === 1
    assert MyConst.a().name() === :a
    assert MyConst.from_name(:a) === MyConst.a()
    assert MyConst.from_value(1) === MyConst.a()
    assert MyConst.value_from_name(:a) === 1
    assert MyConst.name_from_value(1) === :a
    assert MyConst.a() = MyConst.a()
    assert MyConst.a() = match_constant(MyConst.a())
    assert MyConst.name_of_a() = MyConst.a().name()
    assert MyConst.value_of_a() === 1

    assert MyConst.b() === MyConst.B
    assert MyConst.b().value() === 2
    assert MyConst.b().name() === :b
    assert MyConst.from_name(:b) === MyConst.b()
    assert MyConst.from_value(2) === MyConst.b()
    assert MyConst.value_from_name(:b) === 2
    assert MyConst.name_from_value(2) === :b
    assert MyConst.b() = MyConst.b()
    assert MyConst.b() = match_constant(MyConst.b())
    assert MyConst.name_of_b() = MyConst.b().name()
    assert MyConst.value_of_b() === 2

    assert MyConst.c() === MyConst.C
    assert MyConst.c().value() === nil
    assert MyConst.c().name() === :c
    assert MyConst.from_name(:c) === MyConst.c()
    assert MyConst.value_from_name(:c) === nil
    assert MyConst.c() = MyConst.c()
    assert MyConst.c() = match_constant(MyConst.c())
    assert MyConst.name_of_c() = MyConst.c().name()
    assert MyConst.value_of_c() === nil

    assert MyConst.d() === MyConst.D
    assert MyConst.d().value() === 5
    assert MyConst.d().name() === :d
    assert MyConst.from_name(:d) === MyConst.d()
    assert MyConst.from_value(5) === MyConst.d()
    assert MyConst.value_from_name(:d) === 5
    assert MyConst.name_from_value(5) === :d
    assert MyConst.d().d1() === 10
    assert MyConst.d().d2() === 20
    assert_raise UndefinedFunctionError, fn -> MyConst.d().not_exists() end
    assert MyConst.d() = MyConst.d()
    assert MyConst.d() = match_constant(MyConst.d())
    assert MyConst.name_of_d() = MyConst.d().name()
    assert MyConst.value_of_d() === 5

    assert MyConst.e() === MyConst.E
    assert MyConst.e().value() === nil
    assert MyConst.e().name() === :e
    assert MyConst.from_name(:e) === MyConst.e()
    assert MyConst.value_from_name(:e) === nil
    assert MyConst.e().e1() === 30
    assert MyConst.e().e2() === 40
    assert_raise UndefinedFunctionError, fn -> MyConst.e().not_exists() end
    assert MyConst.e() = MyConst.e()
    assert MyConst.e() = match_constant(MyConst.e())
    assert MyConst.name_of_e() = MyConst.e().name()
    assert MyConst.value_of_e() === nil

    assert MyConst.f() === MyConst.F
    assert MyConst.f().value() === nil
    assert MyConst.f().name() === :f
    assert MyConst.from_name(:f) === MyConst.f()
    assert MyConst.value_from_name(:f) === nil
    assert_raise UndefinedFunctionError, fn -> MyConst.f().not_exists() end
    assert MyConst.f() = MyConst.f()
    assert MyConst.f() = match_constant(MyConst.f())
    assert MyConst.name_of_f() = MyConst.f().name()
    assert MyConst.value_of_f() === nil

    assert MyConst.from_name(nil) === nil
    assert MyConst.from_name(:x) === nil

    assert MyConst.from_value(nil) === nil
    assert MyConst.from_value(10) === nil

    assert MyConst.value_from_name(:x) === nil
    assert MyConst.name_from_value(100) === nil

    assert test_is_valid_guard(OtherConst.a())
    assert !test_is_valid_guard(MyConst.a())

    assert test_is_valid_name_guard(:a)
    assert test_is_valid_name_guard(:x)
    assert test_is_valid_name_guard(:y)
    assert !test_is_valid_name_guard(:z)

    assert test_is_valid_value_guard(1)
    assert test_is_valid_value_guard(6)
    assert !test_is_valid_value_guard(100)
    assert !test_is_valid_value_guard(nil)
  end

  defp match_constant(MyConst.a()), do: MyConst.a()
  defp match_constant(MyConst.b()), do: MyConst.b()
  defp match_constant(MyConst.c()), do: MyConst.c()
  defp match_constant(MyConst.d()), do: MyConst.d()
  defp match_constant(MyConst.e()), do: MyConst.e()
  defp match_constant(MyConst.f()), do: MyConst.f()

  defp test_is_valid_guard(constant) when OtherConst.is_valid(constant), do: true
  defp test_is_valid_guard(_), do: false

  defp test_is_valid_name_guard(name) when OtherConst.is_valid_name(name), do: true
  defp test_is_valid_name_guard(_), do: false

  defp test_is_valid_value_guard(value) when OtherConst.is_valid_value(value), do: true
  defp test_is_valid_value_guard(_), do: false
end
