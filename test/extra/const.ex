defmodule Const do
  @moduledoc false

  use Constants, [
    {:a, 1},
    {:b, 2},
    {:c, nil},
    {:d, {5, d1: 10, d2: 20}},
    {:e, [e1: 30, e2: 40]},
    :f
  ]
end
