defmodule OtherConst do
  @moduledoc false

  use Const
  use Other.OtherConst, as: OtherOtherConst

  use Constants, Const.constants() ++ [x: 6] ++ OtherOtherConst.constants()
end
